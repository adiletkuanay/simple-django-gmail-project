from django.test import TestCase

# Create your tests here.
# myapp/tests.py

from django.test import TestCase
from review.models import Post

class PostModelTestCase(TestCase):
    def setUp(self):
        self.post = Post.objects.create(title="Test Post", content="This is a test post.")

    def test_post_creation(self):
        post = Post.objects.get(id=1)
        self.assertEqual(post.title, "Test Post")
        self.assertEqual(post.content, "This is a test post.")

